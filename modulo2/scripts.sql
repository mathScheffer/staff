-- Scripts para limpar o bd
begin
 for deleta in (select sequence_name, 'DROP SEQUENCE '||sequence_name||' ' AS dropar from user_sequences) loop
 BEGIN
  EXECUTE IMMEDIATE deleta.dropar;
  dbms_output.put_line('DROP SEQUENCE '||deleta.sequence_name||' ;');
  EXCEPTION WHEN OTHERS THEN
  dbms_output.put_line('Erro ao tentar dropar a tabela:'||deleta.sequence_name);
 END;
 end loop;
end;


begin
 for deleta in (select table_name, 'DROP TABLE '||table_name||' cascade constraints' AS dropar from user_tables) loop
 BEGIN
  EXECUTE IMMEDIATE deleta.dropar;
  dbms_output.put_line('DROP TABLE '||deleta.table_name||' cascade constraints;');
  EXCEPTION WHEN OTHERS THEN
  dbms_output.put_line('Erro ao tentar dropar a tabela:'||deleta.table_name);
 END;
 end loop;
end;





CREATE TABLE BANCO_DADOS  (
    ID_BANCO_DADOS INT PRIMARY KEY NOT NULL,
    CPF CHAR(11) UNIQUE NOT NULL,
    NOME VARCHAR(255)  DEFAULT 'USUARIO' NOT NULL,
    ANIVERSARIO DATE NULL
    -- ,CONSTRAINT PK_ID_BANCO_DADOS PRIMARY KEY (ID_BANCO_DADOS)
);

CREATE TABLE BANCO_DADOS_DOIS (
    ID_BANCO_DADOS_2 INT PRIMARY KEY NOT NULL,
    ID_BANCO_DADOS INT NOT NULL,
    NOME VARCHAR(255) DEFAULT 'USUARIO' NOT NULL
    ,CONSTRAINT FK_ID_BANCO_DADOS FOREIGN KEY (ID_BANCO_DADOS) REFERENCES BANCO_DADOS(ID_BANCO_DADOS)
    
);

ALTER TABLE BANCO_DADOS_DOIS
    ADD  ENDERECO VARCHAR2(225) NOT NULL;
    
 DROP TABLE BANCO_DADOS; --RETORNA UM ERRO
 DROP TABLE BANCO_DADOS_DOIS;


CREATE SEQUENCE BANCO_DADOS_SEQ
    START WITH 1
--  MAXVALUE 100
    INCREMENT BY 1
--  CYLE        Quando chegar em 100, reseta 
    NOCYCLE; -- Quando chegar em 100, acabou


CREATE SEQUENCE BANCO_DADOS_DOIS_SEQ
START WITH 1
--  MAXVALUE 100
INCREMENT BY 1
--  CYLE        Quando chegar em 100, reseta 
NOCYCLE; -- Quando chegar em 100, acabou

-- C
    INSERT INTO BANCO_DADOS(ID_BANCO_DADOS, CPF, NOME) VALUES (BANCO_DADOS_SEQ.nextval,'89079449078','teste1');
    INSERT INTO BANCO_DADOS(ID_BANCO_DADOS, CPF, NOME) VALUES (BANCO_DADOS_SEQ.nextval,'89179449078','teste2');
    INSERT INTO BANCO_DADOS(ID_BANCO_DADOS, CPF, NOME) VALUES (BANCO_DADOS_SEQ.nextval,'89979449078','teste3');
    INSERT INTO BANCO_DADOS_DOIS(ID_BANCO_DADOS_2, ID_BANCO_DADOS, NOME, ENDERECO)
    VALUES (BANCO_DADOS_DOIS_SEQ.nextval, 1, 'TESTE BANCO DOIS', 'QUALQUER')
    

-- R

    SELECT * FROM BANCO_DADOS;
    SELECT BD.ID_BANCO_DADOS, BD.CPF, BD.NOME FROM BANCO_DADOS BD;
    SELECT COUNT(1) AS contador FROM BANCO_DADOS;



-- Consulta com InnerJoin
SELECT BD.ID_BANCO_DADOS, 
    CPF, 
    BD.NOME,
    ID_BANCO_DADOS_2,
    BD2.NOME AS NOMEBD2,
    ENDERECO
    FROM BANCO_DADOS BD
    INNER JOIN BANCO_DADOS_DOIS BD2 
        ON BD.ID_BANCO_DADOS = BD2.ID_BANCO_DADOS


select BD.ID_BANCO_DADOS, 
    CPF, 
    BD.NOME,
    ID_BANCO_DADOS_2,
    BD2.NOME AS NOMEBD2,
    ENDERECO
    FROM BANCO_DADOS BD
    INNER JOIN BANCO_DADOS_DOIS BD2 
        ON BD.ID_BANCO_DADOS = BD2.ID_BANCO_DADOS
--ON BD.ID_BANCO_DADOS = BD2.ID_BANCO_DADOS AND BD.ID_BANCO_DADOS > 1;
--ON BD.ID_BANCO_DADOS = BD2.ID_BANCO_DADOS OR BD.ID_BANCO_DADOS > 1;
    WHERE BD.NOME = 'TESTE1';

        --lEFT, RIGHT  OU FULL OUTER JOIN
select BD.ID_BANCO_DADOS, 
    CPF, 
    BD.NOME,
    ID_BANCO_DADOS_2,
    BD2.NOME AS NOMEBD2,
    ENDERECO
    FROM BANCO_DADOS BD LEFT JOIN BANCO_DADOS_DOIS BD2 
        ON BD.ID_BANCO_DADOS = BD2.ID_BANCO_DADOS;

        -- CROSS JOIN - PRODUTO CARTESIANO
select BD.ID_BANCO_DADOS, 
    CPF, 
    BD.NOME,
    ID_BANCO_DADOS_2,
    BD2.NOME AS NOMEBD2,
    ENDERECO
    FROM BANCO_DADOS BD cross JOIN BANCO_DADOS_DOIS BD2;

    -- distinct + CROSS JOIN - RETORNA O PRODUTO CARTESIANO E SOMENTE OS DIFERENTES
select distinct BD.NOME  FROM BANCO_DADOS BD CROSS JOIN BANCO_DADOS_DOIS BD2;

select BD.ID_BANCO_DADOS, 
    CPF,
    BD.NOME
    FROM BANCO_DADOS BD 
    UNION
    select ID_BANCO_DADOS_2,
            BD2.NOME NOMEDB2,
            ENDERECO
            FROM BANCO_DADOS_DOIS BD2
-- U
    UPDATE BANCO_DADOS
        SET NOME = 'TESTE'
        where NOME = 'TESTE';
-- D
    delete BANCO_DADOS
        WHERE NOME = 'TESTE3';


-- scripts aula 2

/*ALTER TABLE BANCO_DADOS_DOIS
add CONSTRAINT SYS_C0069*/;93

INSERT INTO BANCO_DADOS_DOIS(ID_BANCO_DADOS_2, ID_BANCO_DADOS, NOME, ENDERECO)
    VALUES (BANCO_DADOS_DOIS_SEQ.NEXTVAL, null, 'TESTE BANCO DOIS 2', 'QUALQUER 2');



        -- Operadores de WHERE
-- =                        : igual
-- <                        : menor que
-- >                        : maior que
-- <>                       : diferente que
-- OR                       : oU
-- AND                      : E
-- IN(x,y...)               : que sejam x,y,...
-- BETWEEN X AND Y          : que ESTEJAM entre x e y
-- NOT : negação
-- LIKE 'X'                 : que seja como X
-- LIKE 'X%'                : que comece com X
-- LIKE '%X'                : que termine com x
-- LIKE '%x%'               : que contenha x no meio da palavra
-- IS NULL / IS NOT NULL     : se é ou não nulo

