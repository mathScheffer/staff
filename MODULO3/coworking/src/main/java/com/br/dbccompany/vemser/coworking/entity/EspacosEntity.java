package com.br.dbccompany.vemser.coworking.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class EspacosEntity extends EntityAbstract<Integer>{
    @Id
    @SequenceGenerator(name="ESPACO_SEQ", sequenceName = "ESPACO_SEQ")
    @GeneratedValue(generator = "ESPACO_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Integer id;

    @Column(nullable = false, unique = true)
    private String nome;

    @Column(nullable = false)
    private int qtdPessoas;

    @Column(nullable = false)
    private double valor;


    @OneToMany(mappedBy = "espaco")
    private List<SaldoClienteEntity> saldosclientes;

    @OneToMany(mappedBy = "espaco")
    private List<EspacosPacotesEntity> espacosPacotes;

    @OneToMany(mappedBy = "espaco")
    private List<ContratacaoEntity> contratacao;


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public List<SaldoClienteEntity> getSaldosclientes() {
        return saldosclientes;
    }

    public void setSaldosclientes(List<SaldoClienteEntity> saldosclientes) {
        this.saldosclientes = saldosclientes;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ContratacaoEntity> getContratacao() {
        return contratacao;
    }

    public void setContratacao(List<ContratacaoEntity> contratacao) {
        this.contratacao = contratacao;
    }
}
