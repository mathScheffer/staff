package com.br.dbccompany.vemser.coworking.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Table( name = "SALDO_x_CLIENTE")
public class SaldoClienteEntity extends EntityAbstract<SaldoClienteId>{

    @EmbeddedId
    private SaldoClienteId id;

    @ManyToOne
    @MapsId("ID_CLIENTE")
    @JoinColumn(name = "ID_CLIENTE", nullable = false)
    private ClienteEntity cliente;

    @ManyToOne
    @MapsId("ID_ESPACOS")
    @JoinColumn(name = "ID_ESPACOS")
    private EspacosEntity espaco;

    @OneToMany(mappedBy = "saldoCliente")
    private List<AcessoEntity> acessos;

    @Enumerated(EnumType.STRING)
    private TipoContratacaoEnum tipoContratacao;

    private double quantidade;

    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date vencimento;

    @OneToMany(mappedBy = "saldoCliente")
    private List<AcessoEntity> acesso;


    @Override
    public SaldoClienteId getId() {
        return null;
    }

    @Override
    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public double getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(double quantidade) {
        this.quantidade = quantidade;
    }

    public Date getVencimento() {
        return vencimento;
    }

    public void setVencimento(Date vencimento) {
        this.vencimento = vencimento;
    }

    public List<AcessoEntity> getAcesso() {
        return acesso;
    }

    public void setAcesso(List<AcessoEntity> acesso) {
        this.acesso = acesso;
    }

    public List<AcessoEntity> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessoEntity> acessos) {
        this.acessos = acessos;
    }
}
