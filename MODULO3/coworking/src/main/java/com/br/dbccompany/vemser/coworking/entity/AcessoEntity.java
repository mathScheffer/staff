package com.br.dbccompany.vemser.coworking.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class AcessoEntity extends EntityAbstract<Integer>{
    @Id
    @SequenceGenerator(name="ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    @GeneratedValue(generator = "ACESSIS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
/*
    @ManyToOne
    @JoinColumn(name="ID_CLIENTE")
    private ClienteEntity idClienteSaldoCliente;

    @ManyToOne
    @JoinColumn(name = "ID_ESPACOS")
    private EspacosEntity idEspacoSaldoCliente;
*/
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "ID_CLIENTES"),
            @JoinColumn(name = "ID_ESPACOS")
    })
    @JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
    private SaldoClienteEntity saldoCliente;

    private boolean isEntrada;

    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date data;

    @Override
    public Integer getId() {
        return null;
    }

    @Override
    public void setId(Integer id) {

    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }
}
