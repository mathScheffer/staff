package com.br.dbccompany.vemser.coworking.controller;

import com.br.dbccompany.vemser.coworking.entity.ContatoEntity;
import com.br.dbccompany.vemser.coworking.entity.TipoContatoEntity;
import com.br.dbccompany.vemser.coworking.services.ContatoService;
import com.br.dbccompany.vemser.coworking.services.TipoContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/tipocontato")
public class TipoContaController extends ControllerAbstract<TipoContatoService, TipoContatoEntity, ContatoEntity>
 {

    @Autowired
    TipoContatoService service;

    @PostMapping( value = "/novo")
    @ResponseBody
    public TipoContatoEntity salvar(@RequestBody TipoContatoEntity entidade) {
        return (TipoContatoEntity) service.salvar(entidade);
    }
}
