package com.br.dbccompany.vemser.coworking.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class EspacosPacotesEntityId implements Serializable {

    @Column(name = "ID_ESPACOS")
    private int idEspacos;

    @Column(name = "ID_PACOTES")
    private int idPacotes;

    public EspacosPacotesEntityId(){}

    public EspacosPacotesEntityId(int idEspacos, int idPacotes) {
        this.idEspacos = idEspacos;
        this.idPacotes = idPacotes;
    }

    public int getIdEspacos() {
        return idEspacos;
    }

    public void setIdEspacos(int idEspacos) {
        this.idEspacos = idEspacos;
    }

    public int getIdPacotes() {
        return idPacotes;
    }

    public void setIdPacotes(int idPacotes) {
        this.idPacotes = idPacotes;
    }
}
