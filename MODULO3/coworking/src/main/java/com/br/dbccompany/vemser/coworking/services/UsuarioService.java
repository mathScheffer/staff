package com.br.dbccompany.vemser.coworking.services;

import com.br.dbccompany.vemser.coworking.cripto.CriptografarMD5;
import com.br.dbccompany.vemser.coworking.entity.UsuarioEntity;
import com.br.dbccompany.vemser.coworking.repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.NoSuchAlgorithmException;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer>{
    @Autowired
    UsuarioRepository repository;
    @Transactional( rollbackFor = Exception.class )
    public UsuarioEntity salvarCriptografada( UsuarioEntity entidade ) throws Exception {
        CriptografarMD5 cripto = new CriptografarMD5(entidade.getSenha());
        entidade.setSenha(cripto.Criptografar());
        return repository.save(entidade);
    }
}
