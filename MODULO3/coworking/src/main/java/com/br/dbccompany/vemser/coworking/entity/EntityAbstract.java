package com.br.dbccompany.vemser.coworking.entity;

public abstract class EntityAbstract<T> {
    public abstract T getId();
    public abstract void setId(T id);
}
