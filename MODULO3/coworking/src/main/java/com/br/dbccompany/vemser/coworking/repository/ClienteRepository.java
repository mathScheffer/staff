package com.br.dbccompany.vemser.coworking.repository;

import com.br.dbccompany.vemser.coworking.entity.ClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClienteRepository extends CrudRepository<ClienteEntity,Integer> {
}
