package com.br.dbccompany.vemser.coworking.entity;


import javax.persistence.*;
import java.io.Serializable;

@Embeddable
public class ContatoEntityId extends EntityAbstract<Integer> implements Serializable {
    //@Id
    @SequenceGenerator(name = "CONTATATO_SEQ", sequenceName = "CONTATATO_SEQ")
    @GeneratedValue(generator = "CONTATATO_SEQ", strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(name = "ID_TIPO_CONTATO")
    private int idTipoContato;

    public ContatoEntityId( ){}

    public ContatoEntityId(int idTipoContato){
        this.idTipoContato = idTipoContato;
    }
    public ContatoEntityId(int id, int idTipoContato) {
        this.id = id;
        this.idTipoContato = idTipoContato;
    }

    @Override
    public void setId(Integer id) {

    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdTipoContato() {
        return idTipoContato;
    }

    public void setIdTipoContato(int idTipoContato) {
        this.idTipoContato = idTipoContato;
    }

    @Override
    public Integer getId() {
        return id;
    }
}

























