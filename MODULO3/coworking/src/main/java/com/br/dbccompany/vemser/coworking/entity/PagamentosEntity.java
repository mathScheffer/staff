package com.br.dbccompany.vemser.coworking.entity;

import javax.persistence.*;

@Entity
public class PagamentosEntity extends EntityAbstract<Integer>{

    @Id
    @SequenceGenerator(name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue(generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "idClientesPacotes")
    private ClientesPacotesEntity clientePacote;

    @ManyToOne
    @JoinColumn(name ="idContratacao")
    private ContatoEntity contratacao;

    @Enumerated(EnumType.STRING)
    TipoPagamentoEnum tipoPagamento;


    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public ClientesPacotesEntity getClientePacote() {
        return clientePacote;
    }

    public void setClientePacote(ClientesPacotesEntity clientePacote) {
        this.clientePacote = clientePacote;
    }

    public ContatoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContatoEntity contratacao) {
        this.contratacao = contratacao;
    }

    public TipoPagamentoEnum getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamentoEnum tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }
}
