package com.br.dbccompany.vemser.coworking.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import java.io.Serializable;

@Entity
@JsonIdentityInfo( generator = ObjectIdGenerators.PropertyGenerator.class, property = "id", scope = ContatoEntity.class)
public class ContatoEntity extends EntityAbstract<ContatoEntityId> implements Serializable {

    @EmbeddedId
    private ContatoEntityId id;
    //private double valorEntrada;
    private String valor;

    @ManyToOne
    @MapsId("id_tipo_contato")
    @JoinColumn(name = "ID_TIPO_CONTATO",nullable = false)
    private TipoContatoEntity tipoContato;

    @ManyToOne
    @JoinColumn(name = "id_cliente")
    private ClienteEntity cliente;

    public ContatoEntity(){}
    public ContatoEntity(ContatoEntityId id, ClienteEntity cliente) {
        this.id = id;
        this.cliente = cliente;
    }

    public ContatoEntityId getId(){
        return this.id;
    }

    public void setId(ContatoEntityId id){
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    /*
    public void setValorFinal(String valorFinal) {
        ConversorDecimal conversor = new ConversorDecimal(valorEntrada);
        this.valorFinal = conversor.converterEmDecimal();
    }*/

}
