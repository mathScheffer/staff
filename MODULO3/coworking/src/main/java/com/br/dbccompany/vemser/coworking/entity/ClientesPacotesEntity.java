package com.br.dbccompany.vemser.coworking.entity;

import javax.annotation.Generated;
import javax.persistence.*;

@Entity
public class ClientesPacotesEntity extends EntityAbstract<Integer>{
    @Id
    @SequenceGenerator(name = "CLIENTES_PACOTES_SEQ", sequenceName = "CLIENTES_PACOTES_SEQ")
    @GeneratedValue(generator="CLIENTES_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @ManyToOne
    //@MapsId("ID_CLIENTE")
    @JoinColumn(name = "ID_CLIENTES")
    private ClienteEntity cliente;

    @ManyToOne
    //@MapsId("ID_PACOTES")
    @JoinColumn(name =  "ID_PACOTES")
    private PacotesEntity pacote;

    private Integer quantidade;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public ClienteEntity getCliente() {
        return cliente;
    }

    public void setCliente(ClienteEntity cliente) {
        this.cliente = cliente;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }


}
