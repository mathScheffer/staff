package com.br.dbccompany.vemser.coworking.repository;

import com.br.dbccompany.vemser.coworking.entity.ContatoEntityId;
import com.br.dbccompany.vemser.coworking.entity.TipoContatoEntity;
import com.br.dbccompany.vemser.coworking.services.TipoContatoService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoContatoRepository
        extends CrudRepository<TipoContatoEntity, ContatoEntityId> {
}
