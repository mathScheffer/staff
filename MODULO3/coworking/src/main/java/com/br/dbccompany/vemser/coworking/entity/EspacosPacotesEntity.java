package com.br.dbccompany.vemser.coworking.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ESPACOS_X_PACOTES")
public class EspacosPacotesEntity extends EntityAbstract<EspacosPacotesEntityId>{

    @EmbeddedId
    private EspacosPacotesEntityId id;

    @ManyToOne
    @MapsId("ID_ESPACOS")
    @JoinColumn(name = "ID_ESPACOS", nullable = false)
    private EspacosEntity espaco;

    @ManyToOne
    @MapsId("ID_PACOTES")
    @JoinColumn(name = "ID_PACOTES", nullable = false)
    private PacotesEntity pacote;

    @Enumerated(EnumType.STRING)
    private TipoContratacaoEnum tipoContratacao;

    private Integer quantidade;

    private Integer prazo;

    @Override
    public EspacosPacotesEntityId getId() {
        return null;
    }

    @Override
    public void setId(EspacosPacotesEntityId id) {

    }

    public EspacosEntity getEspaco() {
        return espaco;
    }

    public void setEspaco(EspacosEntity espaco) {
        this.espaco = espaco;
    }

    public PacotesEntity getPacote() {
        return pacote;
    }

    public void setPacote(PacotesEntity pacote) {
        this.pacote = pacote;
    }

    public TipoContratacaoEnum getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacaoEnum tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public Integer getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(Integer quantidade) {
        this.quantidade = quantidade;
    }

    public Integer getPrazo() {
        return prazo;
    }

    public void setPrazo(Integer prazo) {
        this.prazo = prazo;
    }
}
