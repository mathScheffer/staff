package com.br.dbccompany.vemser.coworking.services;

import com.br.dbccompany.vemser.coworking.entity.ContatoEntity;
import com.br.dbccompany.vemser.coworking.entity.ContatoEntityId;
import com.br.dbccompany.vemser.coworking.entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import com.br.dbccompany.vemser.coworking.repository.TipoContatoRepository;
import org.springframework.stereotype.Service;

@Service
public class TipoContatoService extends ServiceAbstract<TipoContatoRepository,TipoContatoEntity, ContatoEntityId>{

}
