package com.br.dbccompany.vemser.coworking.DTO;

import com.br.dbccompany.vemser.coworking.cripto.CriptografarMD5;
import com.br.dbccompany.vemser.coworking.entity.UsuarioEntity;

import javax.persistence.Column;
import javax.validation.constraints.Size;
import java.security.NoSuchAlgorithmException;

public class UsuarioDTO extends DTOAbstract{

    private String nome;
    @Column(nullable = false, unique = true)
    private String email;
    @Column(nullable = false, unique = true)
    private String login;
    @Column( length = 32)
    @Size(min = 6 )
    private String senha;

    public UsuarioDTO(UsuarioEntity user){
        this.nome = user.getNome();
        this.email = user.getEmail();
        this.login = user.getLogin();
        this.senha = user.getSenha();

    }
    public UsuarioDTO(){}
    public UsuarioEntity convert() throws Exception {
        UsuarioEntity user = new UsuarioEntity();

        user.setNome(this.nome);
        user.setEmail(this.email);
        user.setLogin(this.login);
        user.setSenha(this.senha);
        return user;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) throws Exception {
        CriptografarMD5 cripto = new CriptografarMD5(senha);
        this.senha = cripto.Criptografar();
    }
}
