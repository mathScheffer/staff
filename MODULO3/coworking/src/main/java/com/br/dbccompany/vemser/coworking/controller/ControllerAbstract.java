package com.br.dbccompany.vemser.coworking.controller;

import com.br.dbccompany.vemser.coworking.DTO.DTOAbstract;
import com.br.dbccompany.vemser.coworking.entity.EntityAbstract;
import com.br.dbccompany.vemser.coworking.services.ServiceAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class ControllerAbstract<
        S extends ServiceAbstract,
        E extends EntityAbstract,
        T>{

    @Autowired
    S service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<E> todos(){
        return service.todos();
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public E especifico(@PathVariable Integer id){
        return (E) service.porId(id);
    }
/*
    @PostMapping( value = "/adicionar")
    @ResponseBody
    public D adicionar(@RequestBody D dto){
        E entity = dto.convert();
        D newDto = new D(service.salvar(entity));
        return newDto;
    }
*/
    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public E editar(@PathVariable Integer id, @RequestBody E entidade){
        return (E) service.editar(entidade,id);
    }


}
