package com.br.dbccompany.vemser.coworking.repository;

import com.br.dbccompany.vemser.coworking.entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity,Integer> {
}
