package com.br.dbccompany.vemser.coworking.repository;

import com.br.dbccompany.vemser.coworking.entity.PacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacoteRepository extends CrudRepository<PacotesEntity, Integer> {

}
