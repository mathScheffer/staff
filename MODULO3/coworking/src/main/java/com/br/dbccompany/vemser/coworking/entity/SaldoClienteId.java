package com.br.dbccompany.vemser.coworking.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class SaldoClienteId implements Serializable {

    @Column(name="ID_CLIENTE")
    private int idCliente;

    @Column(name="ID_ESPACOS")
    private int idEspacos;

    public  SaldoClienteId(){}

    public SaldoClienteId(int idCliente, int idEspacos) {
        this.idCliente = idCliente;
        this.idEspacos = idEspacos;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public int getIdEspacos() {
        return idEspacos;
    }

    public void setIdEspacos(int idEspacos) {
        this.idEspacos = idEspacos;
    }


}
