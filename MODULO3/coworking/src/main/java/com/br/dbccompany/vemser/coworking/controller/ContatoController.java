package com.br.dbccompany.vemser.coworking.controller;

import com.br.dbccompany.vemser.coworking.entity.ContatoEntity;
import com.br.dbccompany.vemser.coworking.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/contato")
public class ContatoController extends ControllerAbstract<ContatoService, ContatoEntity, Integer>{
    @Autowired
    ContatoService service;

    @PostMapping( value = "/novo")
    @ResponseBody
    public ContatoEntity salvar(@RequestBody ContatoEntity entidade) {
        return (ContatoEntity) service.salvar(entidade);
    }
}
