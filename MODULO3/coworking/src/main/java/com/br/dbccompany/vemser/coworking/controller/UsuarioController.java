package com.br.dbccompany.vemser.coworking.controller;

import com.br.dbccompany.vemser.coworking.DTO.UsuarioDTO;
import com.br.dbccompany.vemser.coworking.entity.UsuarioEntity;
import com.br.dbccompany.vemser.coworking.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/usuario")
public class UsuarioController extends ControllerAbstract<UsuarioService, UsuarioEntity, Integer>
{
    @Autowired
    UsuarioService service;

    @PostMapping( value = "/adicionar")
    @ResponseBody
    public UsuarioDTO adicionar(@RequestBody UsuarioDTO userDto) throws Exception {
        UsuarioEntity entidade = userDto.convert();
        UsuarioDTO newDto =  new UsuarioDTO(service.salvar(entidade));
        return newDto;
    }
}
