package com.br.dbccompany.vemser.coworking.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import java.sql.Date;
import java.util.List;

@Entity
public class ClienteEntity extends EntityAbstract<Integer>{
    @Id
    @SequenceGenerator(name="CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    @Column(nullable = false)
    private Integer id;

    @Column(nullable = false)
    private String nome;

    @Column(length = 11, columnDefinition = "CHAR", nullable = false, unique = true)
    private String cpf;

    @Column( nullable = false, unique = true)
    @OneToMany(mappedBy = "cliente")
    private List<ContatoEntity> contatos;

    @OneToMany(mappedBy = "cliente")
    private  List<SaldoClienteEntity> saldosClientes;

    @OneToMany(mappedBy = "cliente")
    private List<ClientesPacotesEntity> clientesPacotes;

    @Column(nullable = false)
    @JsonFormat( shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private Date dataNascimento;

    public Integer getId(){
        return this.id;
    }
    public void setId(Integer id){
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }


    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }
}
