package com.br.dbccompany.vemser.coworking.utilities;

import java.text.DecimalFormat;

public class ConversorDecimal {
    private double val;
    private String valorReais;


    public ConversorDecimal(double val){
        DecimalFormat df = new DecimalFormat("#.00");
        String valor = df.format(val);
        this.val = Double.parseDouble(valor);
    }
    //R$ para double
    public ConversorDecimal(String valorReais){

        String formato = valorReais.replace(",",".");
        formato = formato.replace("R$","");

        double formatD = Double.parseDouble(formato);

        DecimalFormat df = new DecimalFormat("#.00");
        String valor = df.format(formatD);

        this.val = Double.parseDouble(valor);

    }

    public String converterEmReais(){

         DecimalFormat format = new DecimalFormat();
         format.setMaximumFractionDigits(2);
         format.setMinimumFractionDigits(2);

         String formatado = "R$"+format.format(this.val);
         formatado = formatado.replace(".",",");
         return formatado;
    }

}
