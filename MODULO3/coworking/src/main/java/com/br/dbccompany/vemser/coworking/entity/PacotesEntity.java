package com.br.dbccompany.vemser.coworking.entity;

import com.br.dbccompany.vemser.coworking.utilities.ConversorDecimal;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacotesEntity extends EntityAbstract<Integer> {

    @Id
    @SequenceGenerator(name = "PACOTE_SEQ", sequenceName = "PACOTE_SEQ")
    @GeneratedValue(generator = "PACOTE_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String valor;


    @OneToMany(mappedBy = "pacote")
    private List<EspacosPacotesEntity> espacosPacotes;

    @OneToMany(mappedBy = "pacote")
    private List<ClientesPacotesEntity> clientesPacotes;

    @OneToMany(mappedBy = "pacote")
    private List<ContratacaoEntity> pacote;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        ConversorDecimal cd = new ConversorDecimal(valor);
        this.valor = cd.converterEmReais();
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ClientesPacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<ContratacaoEntity> getPacote() {
        return pacote;
    }

    public void setPacote(List<ContratacaoEntity> pacote) {
        this.pacote = pacote;
    }
}
