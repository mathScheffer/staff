package com.br.dbccompany.vemser.coworking.repository;

import com.br.dbccompany.vemser.coworking.entity.ContatoEntity;
import com.br.dbccompany.vemser.coworking.entity.ContatoEntityId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContatoRepository extends
        CrudRepository<ContatoEntity, ContatoEntityId> {

}
