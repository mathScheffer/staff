package com.br.dbccompany.vemser.coworking.cripto;
import java.security.*;
import java.math.*;

public class CriptografarMD5 {
    private static MessageDigest m;
    private String senha;
    public CriptografarMD5(String senha){
      this.senha = senha;
    }
    public String Criptografar() throws NoSuchAlgorithmException {

        this.m = MessageDigest.getInstance("MD5");
        this.m.update(this.senha.getBytes(),0,this.senha.length());
        BigInteger senhaCriptografada = new BigInteger(1, m.digest());
        return senhaCriptografada.toString(16);
    }


}
