package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.GerenteEntity;
import br.com.dbccompany.vemser.Repository.GerenteRepository;
import org.springframework.stereotype.Service;

@Service
public class GerenteService extends ServiceAbstract<GerenteRepository, GerenteEntity, Integer> {

    public GerenteEntity gerentePeloCodigo( int codigo ){
        return repository.findByCodigo(codigo);
    }
}
