package br.com.dbccompany.vemser.controller;

import br.com.dbccompany.vemser.Entity.CidadeEntity;
import br.com.dbccompany.vemser.Service.CidadeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/cidade")
public class CidadeController extends ControllerAbstract<CidadeService, CidadeEntity, Integer>{
}
