package br.com.dbccompany.vemser.Security;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //Regras de intercepção
        //1° Chamada http que já está vindo
        //2° Usar os métodos básicos
        http.headers().frameOptions().sameOrigin().and()
                //desabilita todas as permissões ao público
                .csrf().disable().authorizeRequests()
                //Digo que somente este endereço está habilitado para o público
                .antMatchers(HttpMethod.POST, "/login").permitAll()
                .anyRequest().authenticated().and()
                //Quando estiver se logando
                .addFilterBefore(new JWTLoginFilter("/login", authenticationManager()),
                        UsernamePasswordAuthenticationFilter.class)
                //Quando estiver se autenticando
                .addFilterBefore(new JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);

    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        //Como gerencia a autorização
        auth.inMemoryAuthentication()
                .withUser("admin")
                .password("{noop}senha")
                .roles("admin");
    }


}
