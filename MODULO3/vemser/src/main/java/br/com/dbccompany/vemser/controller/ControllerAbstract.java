package br.com.dbccompany.vemser.controller;

import br.com.dbccompany.vemser.Entity.EntityAbstract;
import br.com.dbccompany.vemser.Service.ServiceAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class ControllerAbstract <S extends ServiceAbstract,
        E extends EntityAbstract, T>{

    @Autowired
    S service;
    //localhost:8080+/api/pais+/todos
    //dns+endereçoApi+metodo
    @GetMapping( value = "/todos" )
    //Header && Body
    @ResponseBody
    public List<E> todos() {
        return service.todos();
    }

    //Uma chamada de api é dividida em: Request e Response
    @PostMapping( value = "/novo")
    @ResponseBody
    public E salvar(@RequestBody E entidade){
        return (E) service.salvar(entidade);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public E especifico(@PathVariable Integer id){
        return (E) service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public E editar(@PathVariable Integer id,
                                 @RequestBody E entidade){
        return (E) service.editar(entidade,id);
    }



}
