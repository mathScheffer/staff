package br.com.dbccompany.vemser.controller;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Service.EstadoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/estado")
public class EstadoController extends ControllerAbstract<EstadoService, EstadoEntity, Integer> {


/*
    @Autowired
    EstadoService service;
    //localhost:8080+/api/pais+/todos
    //dns+endereçoApi+metodo
    @GetMapping( value = "/todos" )
    //Header && Body
    @ResponseBody
    public List<EstadoEntity> todosPais() {
        return service.todos();
    }

    //Uma chamada de api é dividida em: Request e Response
    @PostMapping( value = "/novo")
    @ResponseBody
    public EstadoEntity salvar(@RequestBody EstadoEntity estado){
        return  service.salvar(estado);
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public EstadoEntity estadoEspecifico(@PathVariable Integer id){
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public EstadoEntity editarEstado(@PathVariable Integer id,
                                 @RequestBody EstadoEntity estado){
        return service.editar(estado,id);
    }

*/

}
