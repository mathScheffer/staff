package br.com.dbccompany.vemser.Security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Date;

public class TokenAuthenticationService {
    //Não quero instanciá-los
    static final long EXPIRATION_TIME = 860_000_000;

    static final String SECRET ="secret";
    static final String TOKEN_PREFIX = "Bearer";
    static final String HEADER_STRING = "Authrorization";

    //Chamada de Response: Adiciona os dados que a resposta precisa ter
    static void addAuthentication(HttpServletResponse response, String username){
        String JWT = Jwts.builder()
                //corpo do token
                .setSubject(username)
                //data de expiração: a partir de agora + tempo que vai expirar
                .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                //assinatura do token
                .signWith(SignatureAlgorithm.HS512, SECRET)
                //"finaliza" o token
                .compact();
        response.addHeader(HEADER_STRING, TOKEN_PREFIX + " " + JWT);
    }

    //Usar interface sem implementar, chama-se Injeção de Dependencia
    static Authentication getAuthentication(HttpServletRequest request){
        //pega o cabeçalho da requisição com a chave CRIADA
        String token = request.getHeader(HEADER_STRING);
        if( token != null ){
            String user = Jwts.parser()
                    //Para descompactar, preciso da chave
                    .setSigningKey(SECRET)
                    .parseClaimsJws(token.replace(TOKEN_PREFIX, ""))
                    .getBody()
                    .getSubject();
            if( user != null ){
                return new UsernamePasswordAuthenticationToken(user, null, Collections.emptyList());
            }
        }
        return null;
    }
}

