package br.com.dbccompany.vemser.controller;

import br.com.dbccompany.vemser.DTO.PaisDTO;
import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Service.PaisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//Qual endereço da classe no navegador
@Controller
@RequestMapping("/api/pais")
public class PaisController
{
    @Autowired
    PaisService service;
    //localhost:8080+/api/pais+/todos
    //dns+endereçoApi+metodo
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<PaisEntity> todosPais() {
        return service.todos();
    }

    //Uma chamada de api é dividida em: Request e Response
    @PostMapping( value = "/novo")
    @ResponseBody
    public PaisDTO salvar(@RequestBody PaisDTO pais){
        PaisEntity paisEntity = pais.convert();
        PaisDTO newDto = new PaisDTO(service.salvar(paisEntity));
        return  newDto;
    }

    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public PaisEntity paisEspecifico(@PathVariable Integer id){
        return service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public PaisEntity editarPais(@PathVariable Integer id,
                                 @RequestBody PaisEntity pais){
        return service.editar(pais,id);
    }



}
