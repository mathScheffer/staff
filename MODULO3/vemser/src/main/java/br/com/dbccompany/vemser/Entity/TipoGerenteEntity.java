package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;

@Entity
public class TipoGerenteEntity {
    @Id
    @SequenceGenerator(name="TIPO_GERENTE_SEQ", sequenceName = "TIPO_GERENTE_SEQ")
    @GeneratedValue(generator = "TIPO_GERENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private int id;
    private String tipoGerente;
}
