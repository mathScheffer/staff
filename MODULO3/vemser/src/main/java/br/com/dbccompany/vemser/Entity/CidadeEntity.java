package br.com.dbccompany.vemser.Entity;
import javax.persistence.*;
import java.util.List;

@Entity
public class CidadeEntity extends EntityAbstract<Integer> {
    @Id
    @SequenceGenerator(name = "CIDADE_SEQ",sequenceName = "CIDADE_SEQ")
    @GeneratedValue( generator = "CIDADE_SEQ", strategy =  GenerationType.SEQUENCE)
    private  Integer id;
    private String nome;

    @ManyToOne( cascade = CascadeType.ALL )
    @JoinColumn(name = "id_estado")
    private EstadoEntity estado;
    public Integer getId() {
        return id;
    }

    @OneToMany( mappedBy = "cidade")
    private List<EnderecoEntity> enderecos;
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public EstadoEntity getEstado() {
        return estado;
    }

    public void setEstado(EstadoEntity estado) {
        this.estado = estado;
    }

    public List<EnderecoEntity> getEnderecos() {
        return enderecos;
    }

    public void setEnderecos(List<EnderecoEntity> enderecos) {
        this.enderecos = enderecos;
    }
}
