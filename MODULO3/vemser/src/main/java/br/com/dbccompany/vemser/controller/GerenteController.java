package br.com.dbccompany.vemser.controller;

import br.com.dbccompany.vemser.Entity.GerenteEntity;
import br.com.dbccompany.vemser.Service.GerenteService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/gerente")
public class GerenteController

extends ControllerAbstract<GerenteService, GerenteEntity, Integer>{

    @GetMapping( value = "/verPorCodigo/{codigo}")
    @ResponseBody
    public GerenteEntity gerentePeloCodigo(@PathVariable Integer codigo){
        return (GerenteEntity) service.gerentePeloCodigo(codigo);
    }

}
