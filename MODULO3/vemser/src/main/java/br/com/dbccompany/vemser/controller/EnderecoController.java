package br.com.dbccompany.vemser.controller;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Service.EnderecoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/endereco")
public class EnderecoController extends ControllerAbstract<EnderecoService, EnderecoEntity, Integer> {
}
