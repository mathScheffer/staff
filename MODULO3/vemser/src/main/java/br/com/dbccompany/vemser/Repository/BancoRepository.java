package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.BancoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BancoRepository extends CrudRepository<BancoEntity,Integer>{

    BancoEntity findByNome( String nome );
    BancoEntity findByCodigo( int codigo);


}
