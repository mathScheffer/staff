package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class TipoContaEntity extends EntityAbstract<Integer> {
    @Id
    @SequenceGenerator(name="TIPO_CONTA_SEQ", sequenceName = "TIPO_CONTA_SEQ")
    @GeneratedValue(generator = "TIPO_CONTA_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String tipoConta;

    //eEstá sendo usada como chave primaria em  contaentityid
    @OneToMany(mappedBy = "tipoConta")
    private List<ContaEntity> contas;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(String tipoConta) {
        this.tipoConta = tipoConta;
    }

    public List<ContaEntity> getContas() {
        return contas;
    }

    public void setContas(List<ContaEntity> contas) {
        this.contas = contas;
    }
}
