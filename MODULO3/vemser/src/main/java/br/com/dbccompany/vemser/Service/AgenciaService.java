package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Repository.AgenciaRepository;
import org.springframework.stereotype.Service;

@Service
public class AgenciaService extends ServiceAbstract<AgenciaRepository,AgenciaEntity,Integer>{

    public AgenciaEntity agenciaPorNome(String nome)  {
        return repository.findByNome(nome);
    }

}
