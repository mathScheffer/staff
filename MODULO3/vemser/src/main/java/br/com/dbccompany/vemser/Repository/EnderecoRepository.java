package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnderecoRepository extends CrudRepository<EnderecoEntity, Integer> {

    EnderecoEntity findByLogradouro(String logradouro);
    EnderecoEntity findByCep(String cep);
    EnderecoEntity findByBairro(String bairro);
    EnderecoEntity findByNumero(int numero);

}
