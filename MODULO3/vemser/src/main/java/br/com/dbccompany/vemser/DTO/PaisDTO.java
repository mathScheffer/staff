package br.com.dbccompany.vemser.DTO;

import br.com.dbccompany.vemser.Entity.EstadoEntity;
import br.com.dbccompany.vemser.Entity.PaisEntity;

import java.util.List;
//@JsonDeserialize(builder = PaisDTO.VehicleDTOBuilder.class)

public class PaisDTO {
    ///Passo 1: Criar os atributos
    private Integer id;
    private String nome;
    private List<EstadoEntity> estados;

    public PaisDTO(){}
    public PaisDTO( PaisEntity pais ){
        this.id = pais.getId();
        this.nome = pais.getNome();
        this.estados = pais.getEstados();
    }

    //Passo 2: Reescrever o construtor da entidade
    /*Paso 3: Ensinar nosso DTO a criar um objeto tipo Entity.
    * Pra isso, precisamos criar um construtor da nossa Entity²¹ e setar os atributos do nosso DTO nela²
    * e retornar este ³, para que possamos utilizá-lo na camada CONTROLLER4:
    * */
    public PaisEntity convert(){
        PaisEntity pais = new PaisEntity();//¹
        pais.setId(this.id);//²
        pais.setNome(this.nome);//²
        pais.setEstados(this.estados);//²

        return pais;//³
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<EstadoEntity> getEstados() {
        return estados;
    }

    public void setEstados(List<EstadoEntity> estados) {
        this.estados = estados;
    }


}
