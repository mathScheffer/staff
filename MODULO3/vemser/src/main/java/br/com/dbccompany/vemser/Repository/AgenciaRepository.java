package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AgenciaRepository extends CrudRepository<AgenciaEntity, Integer> {

    AgenciaEntity findByNome( String nome );
    AgenciaEntity findByCodigo( int codigo);
    List<AgenciaEntity> findAllByNome( String nome );
}
