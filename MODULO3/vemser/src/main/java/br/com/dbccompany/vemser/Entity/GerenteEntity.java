package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;
/* Herança:
    - Tabelão (campo tipo  referencia tabale filha ) -> pf ou pj como tipos dessa tabela | Single
    --------------------------------------------------------
    - Tabela mãe( Coloca FK dentro das tabelas filhas ) -> Usuario (Mãe e Utilizavel) + Tabelas filhas | JOINED
    - Cria somente as tabelas filhas com todos os campos. -> Usuario (abstrata) + 2 tabelsa (Cliente, Gerente)
Table per class ou Super.


 */
@Entity
//Indica qual será a PK durante o processo da herança
@PrimaryKeyJoinColumn( name = "id")
public class GerenteEntity extends UsuarioEntity implements Serializable {
    /*
    @Id
    @SequenceGenerator(name="GERENTE_SEQ", sequenceName = "GERENTE_SEQ")
    @GeneratedValue(generator = "GERENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private int id;
    */

    private int codigo;

    @Enumerated( EnumType.STRING)
    private TipoGerenteEnum tipoGerente;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable( name = "CONTA_X_GERENTE",
            joinColumns = { @JoinColumn( name = "id_gerente") },
            inverseJoinColumns = {
                    @JoinColumn( name = "id_conta" ),
                    @JoinColumn( name = "id_tipo_conta" )
            }
    )
    private List<ContaEntity> contas;

    public TipoGerenteEnum getTipoGerente() {
        return tipoGerente;
    }

    public void setTipoGerente(TipoGerenteEnum tipoGerente) {
        this.tipoGerente = tipoGerente;
    }

    public List<ContaEntity> getContas() {
        return contas;
    }

    public void setContas(List<ContaEntity> contas) {
        this.contas = contas;
    }
}