package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Repository.EnderecoRepository;
import org.springframework.stereotype.Service;

@Service
public class EnderecoService extends ServiceAbstract<EnderecoRepository, EnderecoEntity, Integer> {

    public EnderecoEntity enderecoPeloLogradouro(String logradouro){
        return repository.findByLogradouro(logradouro);
    }

    public EnderecoEntity EnderecoPeloCep(String cep) {
        return repository.findByCep(cep);
    }

    public EnderecoEntity EnderecoPeloBairro(String bairro){
        return repository.findByBairro(bairro);
    }

    public  EnderecoEntity findByNumero(int numero){
        return repository.findByNumero(numero);
    }

}
