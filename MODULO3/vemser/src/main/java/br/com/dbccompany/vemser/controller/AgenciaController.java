package br.com.dbccompany.vemser.controller;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Service.AgenciaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/Agencia")
public class AgenciaController extends ControllerAbstract<AgenciaService, AgenciaEntity, Integer>{

}
