package br.com.dbccompany.vemser.Entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class ContaEntity extends EntityAbstract<ContaEntityId>implements Serializable {

    @EmbeddedId
    private ContaEntityId id;
    @Column( length = 10)
    private int codigo;
    private double saldo;


    @ManyToOne(cascade =  CascadeType.ALL)
    @JoinColumn(name = "id_agencia")
    private AgenciaEntity agencia;

    @ManyToOne
    //eEstá sendo usada como chave primaria em  contaentityid
    //Ou seja: Só está mapeando aquilo que já foi cliado
    @MapsId("id_tipo_conta")
    @JoinColumn(name = "ID_TIPO_CONTA", nullable = false)
    private TipoContaEntity tipoConta;

    @OneToMany( mappedBy = "conta")
    private List<MovimentacaoEntity> movimentacoes;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable( name = "CONTA_X_GERENTE",
        joinColumns = { @JoinColumn( name = "id_conta"), @JoinColumn(name = "id_tipo_conta") },
        inverseJoinColumns = {
            @JoinColumn( name = "id_gerente")}
    )
    private List<GerenteEntity> gerentes;

    @OneToMany( mappedBy = "conta")
    private List<ContaClienteEntity> contasClientes;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }



    public AgenciaEntity getAgencia() {
        return agencia;
    }

    public void setAgencia(AgenciaEntity agencia) {
        this.agencia = agencia;
    }

    public void setId(ContaEntityId id) {
        this.id = id;
    }

    public ContaEntityId getId() {
        return id;
    }

    public TipoContaEntity getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoContaEntity tipoConta) {
        this.tipoConta = tipoConta;
    }

    public List<MovimentacaoEntity> getMovimentacoes() {
        return movimentacoes;
    }

    public void setMovimentacoes(List<MovimentacaoEntity> movimentacoes) {
        this.movimentacoes = movimentacoes;
    }

    public List<ContaClienteEntity> getContasClientes() {
        return contasClientes;
    }

    public void setContasClientes(List<ContaClienteEntity> contasClientes) {
        this.contasClientes = contasClientes;
    }
}
