package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.BancoEntity;
import br.com.dbccompany.vemser.Repository.BancoRepository;
import org.springframework.stereotype.Service;

@Service
public class BancoService extends ServiceAbstract<BancoRepository,BancoEntity, Integer> {

    public BancoEntity bancoPorCodigo(int codigo)  {
        return repository.findByCodigo(codigo);
    }
    public BancoEntity bancoPorNome(String nome)  {
        return repository.findByNome(nome);
    }

}
