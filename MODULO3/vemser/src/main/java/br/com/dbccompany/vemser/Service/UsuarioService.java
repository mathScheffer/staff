package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import br.com.dbccompany.vemser.Repository.UsuarioRepository;
import org.springframework.stereotype.Service;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository,UsuarioEntity,Integer>{

    public UsuarioEntity usuarioPeloNome( String nome ) {
        return repository.findByNome(nome);
    }

    public UsuarioEntity usuarioPeloCpf( String cpf ) {
        return repository.findByCpf(cpf);
    }

}
