new Agencia
agencia.setCodigo(0001)
agencia.setNome("agenciaWeb")

new Endereco
endereco.logradouro( "rua teste,001" )

Unidirecional = Agencia ( 0001, "agenciaWeb",  endereco ) | Endereco ( "rua teste,001", agencia -> Não vai persistir )
INSERT INTO AGENCIA VALUES (VALORES DA AGENCIA)
Bidirecional = Agencia ( 0001, "agenciaWeb",  endereco ) | Endereco ( "rua teste,001", agencia )
INSERT INTO AGENCIA VALUES (VALORES DA AGENCIA) && INSERT INTO ENDERECO VALUES (VALORES DA ENDERECO)
&& VINCULA OS IDS


***********

Estado - ID_PAIS (RS, 1)(SC, 1)(PR, 1)(SP, 1)
Pais (1, Brasil)

Agencia ID_CONSOLIDACAO (2, AGENCIAWEB, 1)
Consolidacao ID_AGENCIA (1, 2000, R$1500, 2)

Conta
Tabela Intermedia ( ID_CLIENTE, ID_CONTA ) (1, 1)(1, 2)(1, 3)(2, 1)(3, 1)(4, 1)
Cliente