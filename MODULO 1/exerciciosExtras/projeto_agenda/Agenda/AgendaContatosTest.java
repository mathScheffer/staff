import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest{
    @Test
    public void deveBuscarNome(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Matheus","51997630000");
        
        assertEquals("Matheus",agenda.getNome("51997630000"));
    }
    
    @Test
    public void deveBuscarNumero(){
        AgendaContatos agenda = new AgendaContatos();
        agenda.adicionarContato("Matheus","51997630000");
        
        assertEquals("51997630000",agenda.getNumero("Matheus"));
        assertEquals("",agenda.getNumero("Mateus"));
    }
}
