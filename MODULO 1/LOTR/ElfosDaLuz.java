
public class ElfosDaLuz extends Elfo{
    
    private String tipoAtaque;
    private int numeroAtaque;
    private final double QTD_VIDA_GANHA = 10;
    public ElfosDaLuz(String nome){
        super(nome);
        //ao inves de criar assim, oso criar uma arrayList<String> cm ela setada e 
        //acionar o ganharItem dando um .get() para pegar a escrição do item obrigatorio.
        this.ganharItem(new Item(1,"Espada de Galvorn"));
        this.qtdDano = 21.0;
        this.numeroAtaque = 1;
        //this.tipoAtaque = this.numeroAtaque  % 2 == 0 ? "PAR" : "IMPAR";
    }
    
    private boolean devePerderVida(){
            return numeroAtaque % 2 != 0;
    }
    
    private void ganharVida(){
        this.vida += QTD_VIDA_GANHA;
    }
        /* Para separar as responsabilidades (trabalhar com dano e vida),
         * será tirado o else
        else{
            this.curar(10);
        }*/
    public void atacarAnaoComEspada(Dwarf dwarf){
        if(devePerderVida()){
            sofrerDano();
        }else{
            ganharVida();
        }
        dwarf.sofrerDano();
        this.numeroAtaque +=1;
    }
}
