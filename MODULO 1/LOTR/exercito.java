//FAZER PARTE DOIS - BUSCA DE STATUS

import java.util.*;
public class Exercito{
    ArrayList<Elfo> ElfosAlistados;
    //POSSO USAR O TIPO CLASS
    //private finaArrayListl static ArrayList<Class> ELFOS_PERMITIDOS;
    private ArrayList<String> elfosPermitidos;
    {
       elfosPermitidos = new ArrayList<>(Arrays.asList("Elfos Verdes", "Elfos Noturnos"));
       /*private final static ArrayList<String> ELFOS_PERMITIDOS = new ArrayList<>(
        ElfosVerdes.class,
        ElfosNoturnos.class
       );*/
    }
    
    public Exercito(){
        ElfosAlistados = new ArrayList<>();
    }
    
    public boolean isElfoAlistavel(Elfo elfo){
        return (elfo instanceof ElfosNoturnos) || (elfo instanceof ElfosVerdes);
        /* FAZER ASSIM:
         * TIPOS_PERMITIDOS.contains(elfo.getClass());*/
    }
    public Elfo obterElfoAlistado(int pos){
        Elfo r;
        if(pos < ElfosAlistados.size()){
            ElfosAlistados.get(pos);
        }
        return null;
    }
    public ArrayList<Elfo> getElfosAlistados(){
        return this.ElfosAlistados;
    }
    
    
    public boolean alistarElfo(Elfo elfo){
        if(isElfoAlistavel(elfo)){
            ElfosAlistados.add(elfo);
            return true;
        }
        return false;
    }
}
