

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfosNoturnosTest{
    
    @Test
    public void deveAtirarEPerder15DeVida(){
        ElfosNoturnos elfo = new ElfosNoturnos("Noturno");
        Dwarf dwarf = new Dwarf("esquentadinho");
        
        assertEquals(100.0,elfo.getVida(),1e-9);
        
        
        elfo.atirarFlechaNoDwarf(dwarf);
        assertEquals(85.0,elfo.getVida(),1e-9);
    }
    
    @Test
    public void deveAtirarEGanhar3Xp(){
        ElfosNoturnos elfo = new ElfosNoturnos("Noturno");
        Dwarf dwarf = new Dwarf("esquentadinho");
        
        assertEquals(100.0,elfo.getVida(),1e-9);
        
        
        elfo.atirarFlechaNoDwarf(dwarf);
        assertEquals(85.0,elfo.getVida(),1e-9);
        assertEquals(3,elfo.getExperiencia());
    }
    
    @Test
    public void devePararDeAtirarParaNaoMorrer(){
        ElfosNoturnos elfo = new ElfosNoturnos("Noturno");
        Dwarf dwarf = new Dwarf("esquentadinho");

        elfo.getInventario().aumentarItem("Flecha",10);
        
        for(int i = 0; i < 7; i++){
            elfo.atirarFlechaNoDwarf(dwarf);
        }
        
        assertEquals(0.0,elfo.getVida(),1e-9);
        assertEquals(Status.MORTO, elfo.getStatus());
    }
    
}
