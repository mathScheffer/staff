
import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EstatisticasInventarioTest
{
    @Test
    public void EstatisticasInventarioDeveRetornarMedia(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        estatistica.getInventario().adicionarItem(new Item(1,"Arco"));
        estatistica.getInventario().adicionarItem(new Item(5,"flechas"));
        estatistica.getInventario().adicionarItem(new Item(3,"adagas"));
        estatistica.getInventario().adicionarItem(new Item(1,"escudo"));
        estatistica.setMediaInventario();
        assertEquals(2.5, estatistica.getMediaInventario(),0.0001);
    }
    @Test
    public void EstatisticaInventarioDeveRetornarASomaDaFrequencia(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        estatistica.getInventario().adicionarItem(new Item(1,"Arco"));
        estatistica.getInventario().adicionarItem(new Item(5,"flechas"));
        estatistica.getInventario().adicionarItem(new Item(3,"adagas"));
        estatistica.getInventario().adicionarItem(new Item(1,"escudo"));
        assertEquals(10, estatistica.getSomaFrequencia());      
    }
    
    @Test
    public void EstatisticasInventarioParDeveRetornarMediana(){
        Inventario inventario = new Inventario();
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        estatistica.getInventario().adicionarItem(new Item(1,"Arco"));
        estatistica.getInventario().adicionarItem(new Item(2,"flechas"));
        estatistica.getInventario().adicionarItem(new Item(3,"adagas"));
        estatistica.getInventario().adicionarItem(new Item(1,"escudo"));
        estatistica.getInventario().adicionarItem(new Item(1,"poção"));
        estatistica.rol();
        estatistica.setMediana();
        assertEquals(2.0, estatistica.getMediana(),0.001);           
    }
    
        @Test
        public void EstatisticasInventarioDeveRetornarItensAcimaDaMedia(){
        ArrayList<Item> itens = new ArrayList<Item>();
        Inventario inventario = new Inventario();        
        EstatisticasInventario estatistica = new EstatisticasInventario(inventario);
        Item Arco = new Item(1,"Arco");
        Item Flecha = new Item(2,"flechas");
        Item Adaga = new Item(3,"adagas");
        Item Escudo = new Item(1,"escudo");        
        Item Pocao = new Item(1,"poção");        
        estatistica.getInventario().adicionarItem(Arco);
        estatistica.getInventario().adicionarItem(Flecha);
        estatistica.getInventario().adicionarItem(Adaga);
        estatistica.getInventario().adicionarItem(Escudo);
        estatistica.getInventario().adicionarItem(Pocao);
        estatistica.setMediaInventario();
        estatistica.setItensAcimaDaMedia();
        estatistica.setItensAcimaDaMediaDescricao();
        assertEquals("flechas, adagas", estatistica.getItensAcimaDaMediaDescricao());           
    }
}
