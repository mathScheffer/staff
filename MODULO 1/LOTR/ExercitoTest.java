import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ExercitoTest{
    
    @Test
    public void deveAlistar(){
        Exercito exercito = new Exercito();
        ElfosNoturnos ElfoNoturno = new ElfosNoturnos("Elfo Noturno");
        ElfosVerdes ElfoVerde = new ElfosVerdes("Elfo Verde");
        ElfosDaLuz ElfoDaLuz =  new ElfosDaLuz("Elfo da Luz");
        
        exercito.alistarElfo(ElfoNoturno);
        exercito.alistarElfo(ElfoVerde);
        exercito.alistarElfo(ElfoVerde);
        exercito.alistarElfo(ElfoDaLuz);
        
        assertEquals(new ElfosNoturnos("Elfo Noturno"), exercito.getElfosAlistados().get(0));
        assertEquals(new ElfosVerdes("Elfo Verde"), exercito.getElfosAlistados().get(1));
        assertEquals(new ElfosVerdes("Elfo Verde"), exercito.getElfosAlistados().get(2));
        assertEquals(true, exercito.getElfosAlistados().contains(ElfoVerde));
        assertEquals(false,exercito.alistarElfo(ElfoDaLuz));
        assertEquals(3,exercito.getElfosAlistados().size());
        assertEquals(null,exercito.obterElfoAlistado(3));
        //assertEquals(false, exercito.getElfosAlistados().contains(ElfoDaLuz));
        
    }
}
