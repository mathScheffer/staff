
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

    
public class InventarioTest{
    @Test
    public void InventarioDeveAdicionarItem(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem(new Item(1,"espada"));
        assertEquals(1, inventario.getTamanhoInventario());
    }
  
    @Test
    public void InventarioDeveAdicionarTresItens(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem(new Item(1,"espada"));
        inventario.adicionarItem(new Item(1,"escudo"));
        inventario.adicionarItem(new Item(1,"arco"));
        assertEquals(3,inventario.getTamanhoInventario());
        }

    @Test
    public void InventarioDeveRetornarItemDesejado(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"Espada");
        Item escudo = new Item(1,"Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        assertEquals(escudo,inventario.obter(1));
        assertEquals(espada,inventario.obter(0));
        assertEquals(null,inventario.obter(2));
    }

    @Test 
    public void InventarioDeveRetornarItemDescricao(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"Espada");
        Item escudo = new Item(1,"Escudo");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        assertEquals("Espada",inventario.obter(0).getDescricao());
        assertEquals("Escudo",inventario.obter(1).getDescricao());
    }
    
    @Test
    public void InventarioDeveExcluirItemDesejado(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "espada");
        Item escudo = new Item(1, "escudo");
        inventario.adicionarItem(espada);
        inventario.removerSetandoNulo(1);
        assertEquals(espada,inventario.removerERetornar(0));
        assertNull(inventario.obter(0));
        assertNull(inventario.obterPorNome("escudo"));
    }   
    @Test
    public void InventarioDeveFazerListaDeItensPeloNome(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem(new Item(1, "Arco"));
        inventario.adicionarItem(new Item(5, "Flechas"));
        inventario.adicionarItem(new Item(1, "Espada"));
        inventario.adicionarItem(new Item(1, "Escudo"));
        assertEquals("Arco, Flechas, Espada, Escudo", inventario.getItensDescricao());
    }   


    @Test 
    public void InventarioDeveRetornarItemDeMaiorQuantidade(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1,"Espada");
        Item escudo = new Item(1,"Escudo");
        Item flechas = new Item(10,"Flechas");
        Item adagas = new Item(6,"Adaga");
        inventario.adicionarItem(espada);
        inventario.adicionarItem(escudo);
        inventario.adicionarItem(flechas);
        inventario.adicionarItem(adagas);
        assertEquals(flechas,inventario.getItemDeMaiorQuantidade());       
    }
    
        @Test 
        public void InventarioDeveRetornarItemPorNome(){
            Inventario inventario = new Inventario();
            Item espada = new Item(1,"Espada");
            Item escudo = new Item(1,"Escudo");
            inventario.adicionarItem(espada);
            inventario.adicionarItem(escudo);
            assertEquals(escudo,inventario.obterPorNome("Escudo"));
    }
    
    @Test public void InventarioParDeveInverterItens(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem(new Item(1, "Arco"));
        inventario.adicionarItem(new Item(5, "Flechas"));
        inventario.adicionarItem(new Item(1, "Espada"));
        inventario.adicionarItem(new Item(1, "Escudo"));
        assertEquals("Arco, Flechas, Espada, Escudo", inventario.getItensDescricao());
        inventario.inverteInventario();
        assertEquals("Escudo, Espada, Flechas, Arco", inventario.getItensDescricao());
    }
    @Test 
    public void InventarioImparDeveInverterItens(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem(new Item(1, "Arco"));
        inventario.adicionarItem(new Item(5, "Flechas"));
        inventario.adicionarItem(new Item(1, "Espada"));
        inventario.adicionarItem(new Item(1, "Escudo"));
        inventario.adicionarItem(new Item(1, "Cajado"));
        assertEquals("Arco, Flechas, Espada, Escudo, Cajado", inventario.getItensDescricao());
        inventario.inverteInventario();
        assertEquals("Cajado, Escudo, Espada, Flechas, Arco", inventario.getItensDescricao());
    }
    @Test 
    public void InventarioOrdenarPorQuantidade(){
        Inventario inventario = new Inventario();
        inventario.adicionarItem(new Item(1, "Arco"));
        inventario.adicionarItem(new Item(5, "Flechas"));
        inventario.adicionarItem(new Item(2, "Espada"));
        inventario.adicionarItem(new Item(1, "Escudo"));
        inventario.adicionarItem(new Item(4, "adaga"));
        assertEquals("Arco, Flechas, Espada, Escudo, adaga", inventario.getItensDescricao());
        inventario.ordenarItens();
        assertEquals("Arco, Escudo, Espada, adaga, Flechas", inventario.getItensDescricao());
    }
     @Test 
    public void InventarioOrdenarPoTipoDeOrdenacao(){
        Inventario inventario = new Inventario();
        tipoOrdenacao ordenacao;
        inventario.adicionarItem(new Item(1, "Arco"));
        inventario.adicionarItem(new Item(5, "Flechas"));
        inventario.adicionarItem(new Item(2, "Espada"));
        inventario.adicionarItem(new Item(1, "Escudo"));
        inventario.adicionarItem(new Item(4, "adaga"));
        assertEquals("Arco, Flechas, Espada, Escudo, adaga", inventario.getItensDescricao());
        
        inventario.orderItensPorTipo(tipoOrdenacao.ASC);
        assertEquals("Arco, Escudo, Espada, adaga, Flechas", inventario.getItensDescricao());
        assertEquals(tipoOrdenacao.ASC,inventario.getTipoOrdenacao());
        
        inventario.orderItensPorTipo(tipoOrdenacao.DESC);
        assertEquals("Flechas, adaga, Espada, Arco, Escudo", inventario.getItensDescricao());
        assertEquals(tipoOrdenacao.DESC,inventario.getTipoOrdenacao());
    }
    
}
