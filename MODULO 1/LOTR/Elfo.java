import java.util.*;
public class Elfo extends Personagem{
    private Dwarf anao = new Dwarf("ZePequeno");
    private int indiceFlecha;
    private int indiceArco;
    protected String tipoDeElfo;
    // https://www.guj.com.br/t/contador-de-instancias/47407/6
    private static int instancias = 0;
    {
        this.indiceFlecha = 1;
        this.indiceArco = 0;

    }
    public Elfo(String nome){
        super(nome);
        this.vida = 100.0;
        this.inventario.adicionarItem(new Item(1, "Arco"));
        this.inventario.adicionarItem(new Item(2, "Flecha"));
        //Elfo elfo;
        //tiposDeElfo.put("Elfo da Luz",new ElfosDaLuz("Elfo da Luz"));
        //tiposDeElfo.put("Elfo Noturno",new ElfosNoturnos("Elfo Noturno"));
        this.xpPorAtaque = 1;
        ++instancias;
    }
    @Override
    protected void finalize(){
        --instancias;
    } 
    
    public int getInstancias(){
        return this.instancias;
        // return elfo.instancias
    }
    
    public Inventario getInventario(){
        return this.inventario;
    }
    
    
    public String getTipoDeElfo(){
        return this.tipoDeElfo;
    }
    public Item getFlecha(){
           return this.inventario.obter(indiceFlecha);
    }
    public Item getArco(){
        return this.inventario.obter(indiceArco);
    }
    //Retorna o total de flechas com getQuantidade()
    public int getQtdFlechas(){
        return this.getFlecha().getQuantidade();
    }
    public boolean podeAtirarFlecha() {
        return this.getQtdFlechas() > 0;
    }
    protected void atirarFlechaNoDwarf(Dwarf dwarf){
        int qtdAtual = this.getQtdFlechas();
        if ( podeAtirarFlecha()) {
            this.getFlecha().setQuantidade(qtdAtual - 1);
            this.aumentarXP();
            this.sofrerDano();
            dwarf.sofrerDano();
        }
    }
    public boolean equals(Object elfo) {
        Elfo outroElfo = (Elfo) elfo;
        return this.tipoDeElfo == outroElfo.getTipoDeElfo();
    }
}




















