
public class ElfosVerdes extends Elfo{

    {

    }
    public ElfosVerdes(String nome){
        super(nome);
        this.xpPorAtaque = 2;
    }
    @Override
    public int getExperiencia(){
        return this.experiencia;
    }


    /*Usar uma ArrayList<String> ESTATICA = new ArrayList<String>(Arrays.asList("param1","param2")) 
       
       */
    @Override
    public void ganharItem(Item item){
        if(item.getDescricao().equals("Espada de aço valiriano") ||
        item.getDescricao().equals("Arco de Vidro") ||
        item.getDescricao().equals("Flecha de Vidro")){  
            this.inventario.adicionarItem(item);
        }
    }   
    
    @Override
    public void perderItem(Item item){
        if(item.getDescricao().equals("Espada de aço valiriano") ||
        item.getDescricao().equals("Arco de Vidro") ||
        item.getDescricao().equals("Flecha de Vidro")){ 
            this.inventario.removerPorItem(item);
        }        
    }
}
