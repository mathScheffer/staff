    
import java.util.*;

public class Inventario extends Object{
   private ArrayList<String> itensPermanentes;
   private ArrayList<Item> itens;
   private tipoOrdenacao ordenacao;
   

   public Inventario(){
       itens = new ArrayList<Item>();
   }
   
   public void getItens(){
       //return this.itens.toArray();
       for(int i = 0; i < this.itens.size(); i++){
           System.out.println(this.itens.get(i).getDescricao());
       }
   }
   public int getTamanhoInventario(){
       return this.itens.size();
   }

   public void adicionarItem(Item item){
       this.itens.add(item);
    }
   public void adicionarVariosItens(){
    adicionarItem(new Item(1, "Arco"));
    adicionarItem(new Item(5, "Flechas"));
    adicionarItem(new Item(2, "Espada"));
    adicionarItem(new Item(1, "Escudo"));
    adicionarItem(new Item(4, "adaga"));
    }

   public Item obter(int posicao){

        Item r;
        if(posicao < this.itens.size()){

            r = this.itens.get(posicao);
        }else{
            r = null;
        }
        return r;
        
   }
   
   public Item obterPorNome(String descricao){
       //usar forEach
       int i = 0;
       while( i < this.itens.size()){
           if(this.itens.get(i).getDescricao() == descricao){
               break;
           }
           i++;
       }
       return this.obter(i);
   }
   public Item obterPorItem(Item item){
       String descricaoAtual = item.getDescricao();
       return obterPorNome(descricaoAtual);
   }
   //retorna o item que excluiu
   public Item removerERetornar(int posicao) {
        return this.itens.remove(posicao);
   }
   
   public void removerSetandoNulo(int posicao){
       if(posicao < this.itens.size()){
           this.itens.set(posicao, null);
       }
   }
   public void removerPorItem(Item item){
       this.itens.remove(item);
   }
   public String getItensDescricao() {
       /*Posso usar foreach para economizar código se e somente se, não removermos
         itens*/
       /*
       for()
        */
       StringBuilder descricoes = new StringBuilder();
       for(int i = 0; i < this.itens.size(); i++){
           if(this.itens.get(i) != null){
               String descricao = this.itens.get(i).getDescricao();
               descricoes.append(descricao);
               if(i < this.itens.size() - 1) {
                   descricoes.append(", ");
               }
            }
       }
       return descricoes.toString();
   }

    public Item getItemDeMaiorQuantidade(){ 
        int indice = 0, itemComMaisQtd = 0;
        for(int i = 0; i < this.itens.size(); i++){
            int qtdAtual = this.itens.get(i).getQuantidade();
            if(qtdAtual > itemComMaisQtd){
                itemComMaisQtd = qtdAtual;
                indice = i;
            }
        }
        return this.itens.size() > 0 ? this.obter(indice) : null;
    }
    
    public void inverteInventario(){
        /*
         ArrayList<Item> listaInvertida = new ArrayList<>(this.itens.size());
         
         for( int i = this.itens.size() - 1 ;i >= 0; i--){
             listaInvertida.add(this.itens.get(i));
         }
         this.itens = inverteInventario;
         */
        if(this.itens.size() % 2 == 0){
            int i = 0;
            int j = this.itens.size() - 1;
            while(i < this.itens.size()/2 && j >= this.itens.size()/2){
            
                    Item reserva = this.itens.get(j);
                    this.itens.set(j, this.itens.get(i));
                    this.itens.set(i, reserva);
                    i++;
                    j--;
            }
        }else{
            int posicaoCentral = ((this.itens.size()+1)/2) - 1;
            int j = this.itens.size() - 1;
            int i = 0;
            while(i < posicaoCentral && j > posicaoCentral){
                Item reserva = this.itens.get(j);
                this.itens.set(j, this.itens.get(i));
                this.itens.set(i, reserva);
                i++;
                j--;
            }
        }
    }
    public void ordenarItens(){       
        for(int i = 1; i < this.itens.size(); i++){
            Item reserva = this.itens.get(i);
            int j = i - 1;
            while(j >=0 && reserva.getQuantidade() < this.itens.get(j).getQuantidade() ){
                this.itens.set(j+1,itens.get(j));
                j--;
            }
            this.itens.set(j+1,reserva);
        }
    }
    public void orderItensPorTipo(tipoOrdenacao ordenacao){
        if(ordenacao == tipoOrdenacao.ASC){
            for(int i = 1; i < this.itens.size(); i++){
                Item reserva = this.itens.get(i);
                int j = i - 1;
                while(j >=0 && reserva.getQuantidade() < this.itens.get(j).getQuantidade() ){
                    this.itens.set(j+1,itens.get(j));
                    j--;
                }
                this.itens.set(j+1,reserva);
            }
            this.ordenacao = tipoOrdenacao.ASC;
        }else if(ordenacao == tipoOrdenacao.DESC){
            for(int i = 1; i < this.itens.size(); i++){
                Item reserva = this.itens.get(i);
                int j = i - 1;
                while(j >=0 && reserva.getQuantidade() > this.itens.get(j).getQuantidade() ){
                    this.itens.set(j+1,itens.get(j));
                    j--;
                }
                this.itens.set(j+1,reserva);
            }
            this.ordenacao = tipoOrdenacao.DESC;            
        }
    }
    public tipoOrdenacao getTipoOrdenacao(){
            return this.ordenacao;
    }
    
    public void aumentarItem(String itemBuscado,int qtd){
        Item   item = this.obterPorNome(itemBuscado);
        item.setQuantidade(qtd + item.getQuantidade());    
    }
}
