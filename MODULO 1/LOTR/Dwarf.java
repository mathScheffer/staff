     
public class Dwarf extends Personagem{
    // private EquipamentoStatus statusIten;
    private Item Escudo;
    {
        this.inventario = new Inventario();

    }
    public Dwarf(String nome){
        super(nome);
        this.vida = 110;
        this.Escudo = new Item(1,"Escudo");
        this.inventario.adicionarItem(Escudo);
        this.qtdDano = 10.0;
    }
    protected double calcularDano(){
        return this.Escudo.getItemStatus() == EquipamentoStatus.EQUIPADO ? 5.0 : this.qtdDano; 
    }
    /*
    public void setFlechada(double flechada){
        this.vida = this.getVida() - (flechada * 10);
    }*/
    private boolean podeSofrerDano(){
        return this.vida > 0;
    }
    //Setar booleano o
    public String manusearEscudo(EquipamentoStatus status){
        String r = new String();
        this.Escudo.setItemStatus(status);
        if(this.Escudo.getItemStatus() == EquipamentoStatus.EQUIPADO){
            r = "Item equipado!";
        }else{
            r = "Item Desequipado!";
        }
        return r;
    }
    
    public EquipamentoStatus getEscudoStatus(){
        return this.Escudo.getItemStatus();
    }
    public void sofrerDano(){
        //double vidaAtual = this.vida;
        if( podeSofrerDano()){
            //this.vida -= 10.0;
            if(this.vida-10 <= 0){
                this.vida = 0;
                this.status = Status.MORTO;
            }else if (this.Escudo.getItemStatus() == EquipamentoStatus.DESEQUIPADO){
                this.vida-=10;
                this.status = Status.SOFREU_DANO;
            }else{
                this.vida-=5;
                this.status = Status.SOFREU_DANO_REDUZIDO;
            }
        }
    }
    public Status getStatus(){
        return this.status;
    }
}

