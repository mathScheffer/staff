    
    
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class DwarfTest{
    @Test
    public void dwarfNasceCom110DeVida(){
        Dwarf dwarfQualquer = new Dwarf("Anao");
        assertEquals(110.0, dwarfQualquer.getVida(), 0.000001);
    }
    @Test
    public void deveReceber10deDano(){
        Dwarf dwarfQualquer = new Dwarf("Anao");
        dwarfQualquer.sofrerDano();
        assertEquals(100.0, dwarfQualquer.getVida(), 0.01);
        assertEquals(Status.SOFREU_DANO, dwarfQualquer.getStatus());
    }
    
    @Test
    public void deveZerarVida(){
        Dwarf dwarf = new Dwarf("Anao");
        
        for(int i = 0; i < 12; i++){
            dwarf.sofrerDano();
        }
        assertEquals(0.0, dwarf.getVida(), 0.01);
        assertEquals(Status.MORTO, dwarf.getStatus());
    }
    
    @Test
    public void deveGarantirQueNaoIraReceberDanoQuandoMorrer(){
        Dwarf dwarf = new Dwarf("Anao");
        for(int i = 0; i <= 13; i++){
            dwarf.sofrerDano();
        }
        assertEquals(0,dwarf.getVida(), 0.001);

    }
         
    @Test
    public void anaoDeveEquiparEscudo(){
        Dwarf dwarf = new Dwarf("Anao");
        dwarf.getEscudoStatus();
        assertEquals(EquipamentoStatus.DESEQUIPADO,dwarf.getEscudoStatus() );
        
        dwarf.manusearEscudo(EquipamentoStatus.EQUIPAR);
        
        assertEquals(EquipamentoStatus.EQUIPADO, dwarf.getEscudoStatus());
        
    }
    
    @Test
    public void anaoDeveDesequiparEscudo(){
        Dwarf dwarf = new Dwarf("Anao");
        
        dwarf.manusearEscudo(EquipamentoStatus.EQUIPAR);
    
        assertEquals(EquipamentoStatus.EQUIPADO, dwarf.getEscudoStatus());
        
        dwarf.manusearEscudo(EquipamentoStatus.DESEQUIPAR);
        assertEquals(EquipamentoStatus.DESEQUIPADO, dwarf.getEscudoStatus());
          
    }
    
    @Test 
    public void anaoDeveSofrerDanoReduzido(){
        Dwarf dwarf = new Dwarf("Anao");
        
        dwarf.manusearEscudo(EquipamentoStatus.EQUIPAR);    
        assertEquals(EquipamentoStatus.EQUIPADO, dwarf.getEscudoStatus()); 
        dwarf.sofrerDano();
        assertEquals(105.0,dwarf.getVida(),00001);
        assertEquals(Status.SOFREU_DANO_REDUZIDO, dwarf.getStatus());
    }
    @Test 
    public void anaoDeveSofrerDanoReduzidoTresVezes(){
        Dwarf dwarf = new Dwarf("Anao");
        
        dwarf.manusearEscudo(EquipamentoStatus.EQUIPAR);    
        assertEquals(EquipamentoStatus.EQUIPADO, dwarf.getEscudoStatus()); 
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        assertEquals(95.0,dwarf.getVida(),00001);
        assertEquals(Status.SOFREU_DANO_REDUZIDO, dwarf.getStatus());
    }
    @Test 
    public void anaoEquiparEscudoDepoisDeSofrerDano(){
        Dwarf dwarf = new Dwarf("Anao");
        
        assertEquals(EquipamentoStatus.DESEQUIPADO,dwarf.getEscudoStatus());
        dwarf.sofrerDano();
        dwarf.sofrerDano();
        assertEquals(Status.SOFREU_DANO,dwarf.getStatus());
        assertEquals(90.0,dwarf.getVida(),0.00001);
        
        dwarf.manusearEscudo(EquipamentoStatus.EQUIPAR);    
        assertEquals(EquipamentoStatus.EQUIPADO, dwarf.getEscudoStatus()); 

        dwarf.sofrerDano();
        assertEquals(85.0,dwarf.getVida(),00001);
        assertEquals(Status.SOFREU_DANO_REDUZIDO, dwarf.getStatus());
    }
}
