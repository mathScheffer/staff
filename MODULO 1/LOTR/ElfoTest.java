

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest{
    @Test
    public void elfoDeveNascerCom2FlechasEUmArco(){
        Elfo elfoQualquer = new Elfo("Legolas");
        assertEquals(2, elfoQualquer.getQtdFlechas());
        assertEquals(1, elfoQualquer.getArco().getQuantidade());
    }
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXP(){
        Elfo elfoQualquer = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Malungrid");
        //Acao
        elfoQualquer.atirarFlechaNoDwarf(anao);
        elfoQualquer.atirarFlechaNoDwarf(anao);
        elfoQualquer.atirarFlechaNoDwarf(anao);
        //Verificacao
        assertEquals(0, elfoQualquer.getQtdFlechas());
        assertEquals(2, elfoQualquer.getExperiencia());
        assertEquals(90.0, anao.getVida(), .00001);
    }
    @Test
    public void atirarFlechaDEmDwarfTiraVida(){
        Elfo elfoQualquer = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Malungrid");
        //Acao
        elfoQualquer.atirarFlechaNoDwarf(anao);
        //Verificacao
        assertEquals(1, elfoQualquer.getQtdFlechas());
        assertEquals(1, elfoQualquer.getExperiencia());
        assertEquals(100.0, anao.getVida(), .00001);
    }
    
    @Test
    public void elfoDeveNascerComArcoEFlecha(){
       Elfo elfoQualquer = new Elfo("Legolas");
       assertEquals(new Item(1,"Arco"),elfoQualquer.getArco());
       assertEquals(new Item(2,"Flecha"),elfoQualquer.getFlecha());
    }
    
    @Test
    public void calcularINstancias(){
        Elfo elfo = new Elfo("elfo");
        ElfosNoturnos elfo2 = new ElfosNoturnos("elfo");
        ElfosVerdes elfo3 = new ElfosVerdes("Elfo 3");
        assertEquals(3,elfo.getInstancias());
        elfo2.finalize();
        elfo.finalize();
        assertEquals(1,elfo.getInstancias());
    }
}
































