
public class Item{

    String descricao;
    private int quantidade;
    private EquipamentoStatus status;

    public Item(int quantidade, String descricao){
        this.quantidade = quantidade;
        this.descricao = descricao;
        this.status = EquipamentoStatus.DESEQUIPADO;
    }
    public int getQuantidade(){
        return this.quantidade;
    }
    //void = nao tem retorno
    public void setQuantidade(int quantidade){
        this.quantidade = quantidade;
        
    }
    
    public void setItemStatus(EquipamentoStatus status){
       //String r = new String();
        if(status == EquipamentoStatus.EQUIPAR){
            this.status = EquipamentoStatus.EQUIPADO;
            //r = "Item Equipado";
        }else if(status == EquipamentoStatus.DESEQUIPAR){
            this.status = EquipamentoStatus.DESEQUIPADO;
            //r =  "Item Desequipado";
        }
        //return r;
    }
    
    public EquipamentoStatus getItemStatus(){
        return this.status;
    }
    public String getDescricao(){
        return this.descricao;
    }
    //Necesário para que saibamos que o elfo nasceu com arco e flecha
    public boolean equals(Object obj) {
        Item outroItem = (Item) obj;
        return this.quantidade == outroItem.getQuantidade() &&
                this.descricao == outroItem.getDescricao();
    }
}
