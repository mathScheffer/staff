

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfosVerdesTest {

    @Test
    public void deveNascerCom2Flechas(){
        Elfo elfoQualquer = new Elfo("Legolas");
        //Verificaca
        assertEquals(2, elfoQualquer.getQtdFlechas());
    }
    
    @Test
    public void deveGanhar2XP(){
        Elfo elfo = new Elfo("elfo");
        elfo.aumentarXP();
        assertEquals(2, elfo.getExperiencia());
    }
    @Test
    public void deveGanharItensEspecificos(){
        ElfosVerdes elfoVerde = new ElfosVerdes("Legolas");
        Item EspadaValiriano = new Item(1,"Espada de aço valiriano");
        Item ArcoVidro = new Item(1,"Arco de Vidro");
        Item FlechaVidro = new Item(5,"Flecha de Vidro");
        Item Escudo = new Item(1, "Escudo");
        elfoVerde.ganharItem(EspadaValiriano);
        elfoVerde.ganharItem(ArcoVidro);
        elfoVerde.ganharItem(FlechaVidro);
        
        elfoVerde.ganharItem(Escudo);
        
        assertEquals(new Item(1,"Arco"), elfoVerde.getInventario().obter(0));
        assertEquals(new Item(2,"Flecha"),elfoVerde.getInventario().obter(1));
        assertEquals(EspadaValiriano,elfoVerde.getInventario().obter(2));
        assertEquals(ArcoVidro,elfoVerde.getInventario().obter(3));
        assertEquals(FlechaVidro,elfoVerde.getInventario().obter(4));        
        
        assertNull(elfoVerde.getInventario().obter(5));
        
    }
    @Test
    public void devePerderItensEspecificos(){
        ElfosVerdes elfoVerde = new ElfosVerdes("Legolas");
        Item EspadaValiriano = new Item(1,"Espada de aço valiriano");
        Item ArcoVidro = new Item(1,"Arco de Vidro");
        Item FlechaVidro = new Item(5,"Flecha de Vidro");
        
        elfoVerde.ganharItem(EspadaValiriano);
        elfoVerde.ganharItem(ArcoVidro);
        elfoVerde.ganharItem(FlechaVidro);
        
        assertEquals(new Item(1,"Arco"), elfoVerde.getInventario().obter(0));
        assertEquals(new Item(2,"Flecha"),elfoVerde.getInventario().obter(1));
        assertEquals(EspadaValiriano,elfoVerde.getInventario().obter(2));
        assertEquals(ArcoVidro,elfoVerde.getInventario().obter(3));
        assertEquals(FlechaVidro,elfoVerde.getInventario().obter(4));
        
    }
}
