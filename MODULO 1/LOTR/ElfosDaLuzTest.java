import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
public class ElfosDaLuzTest{
   
    @Test
    public void deveNascerComEspadaDeGalvorn(){
        ElfosDaLuz elfo = new ElfosDaLuz("Elfo da Luz");
        
        assertEquals(new Item(1, "Espada de Galvorn"), elfo.getInventario().obter(2));
        
    }
    
    @Test 
    public void devePerder21ECurar10AoAtacar(){
        ElfosDaLuz elfo = new ElfosDaLuz("Elfo da Luz");
        elfo.atacarAnaoComEspada(new Dwarf("Anao"));
        //elfo.atacarAnaoComEspada(new Dwarf("Anao"));
        assertEquals(79.0, elfo.getVida(),1e-9);
        
        elfo.atacarAnaoComEspada(new Dwarf("Anao"));
        assertEquals(89.0, elfo.getVida(),1e-9);
    }

    
}
