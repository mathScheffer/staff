import java.util.ArrayList;
public class PaginadorInventario{
    private Inventario inventario;
    private int marcador;
    private ArrayList<Item> item;
    public PaginadorInventario(Inventario inventario){
        this.inventario = new Inventario();
    }
    public Inventario getInventario(){
        return this.inventario;
    }
    public void adicionaItens(){
        this.inventario.adicionarItem(new Item(1,"Arco"));
        this.inventario.adicionarItem(new Item(2,"flechas"));
        this.inventario.adicionarItem(new Item(3,"adagas"));
        this.inventario.adicionarItem(new Item(1,"escudo"));
        this.inventario.adicionarItem(new Item(1,"poção"));
        //inventario.getTamanhoInventario();
    }
    
    public void pular(int marcador){
        this.marcador = marcador > 0 ? marcador : 0;
    }
    public ArrayList<Item> limitar(int limite){
        if(this.marcador < this.inventario.getTamanhoInventario()){
            this.item = new ArrayList<Item>();
            int i = this.marcador;
            limite = limite - 1;//2
            while(this.inventario.obter(i) != null && limite >=0){
                item.add(inventario.obter(i));
                i++;//3
                limite--;//1
            }
        }
        return this.item;
    }
}
