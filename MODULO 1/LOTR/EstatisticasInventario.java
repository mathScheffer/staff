import java.util.ArrayList;  
public class EstatisticasInventario{
    private Inventario inventario;
    private double mediana;
    private int frequencia;
    private double media;
    private ArrayList<Integer> rol;
    private ArrayList<Item> itensAcimaMedia;
    private String itensAcimaMediaDescricao;
    {
    
    }
    public EstatisticasInventario(Inventario inventario){
        this.inventario = new Inventario();
        
    }
    public int getSomaFrequencia(){
        int somaVolume = 0;
        int qtdItens =  inventario.getTamanhoInventario();
        for(int i = 0; i < qtdItens; i++){
            somaVolume += inventario.obter(i).getQuantidade();
        }
        this.frequencia = somaVolume;
        return this.frequencia;
    }
    public void adicionaItens(){
        this.inventario.adicionarItem(new Item(1,"Arco"));
        this.inventario.adicionarItem(new Item(2,"flechas"));
        this.inventario.adicionarItem(new Item(3,"adagas"));
        this.inventario.adicionarItem(new Item(1,"escudo"));
        this.inventario.adicionarItem(new Item(1,"poção"));
        //inventario.getTamanhoInventario();
    }
    public Inventario getInventario(){
        return this.inventario;
    }
    public void setMediaInventario(){
        double somaVolume = 0;
        int qtdItens =  inventario.getTamanhoInventario();
        for(int i = 0; i < qtdItens; i++){
            somaVolume += inventario.obter(i).getQuantidade();
        }
        this.media = somaVolume/qtdItens;
    }
    
    public double getMediaInventario(){
        return this.media;
    }
    
    public void rol (){
        this.rol  = new ArrayList<Integer>();
        for(int i = 0; i < this.inventario.getTamanhoInventario();i++){
            
            for(int j = 0;j < this.inventario.obter(i).getQuantidade();j++){
                rol.add(i);

            }
        }
    }
    public void escreverRol(){
        for(int i = 0; i < rol.size(); i++){
            System.out.println(rol.get(i));
        }
    }
    public void setMediana(){
        if(rol.size()%2 == 0 ){
            int esq = (rol.size() / 2) - 1; 
            int dir = rol.size()/ 2;
            double medianaPar = (rol.get(esq)+rol.get(dir))/2;
            this.mediana = medianaPar;
        }else{
            int centro = ((rol.size()+1)/2) - 1;
            this.mediana = rol.get(centro);
        }    
    }
    public double getMediana(){
        return this.mediana;
    }
    
    public void setItensAcimaDaMedia(){
        this.itensAcimaMedia = new ArrayList<Item>();
        for(int i = 0; i < this.inventario.getTamanhoInventario(); i++){
            if(this.inventario.obter(i).getQuantidade() > this.media){
                itensAcimaMedia.add(this.inventario.obter(i));
            }
        }
    }
    public ArrayList<Item> getItensAcimaDaMedia(){
        return this. itensAcimaMedia;
    }
    public void setItensAcimaDaMediaDescricao(){
        StringBuilder desc = new StringBuilder();
        for(int i = 0  ; i < this.itensAcimaMedia.size(); i++){
            desc.append(itensAcimaMedia.get(i).getDescricao());
            if(i < this.itensAcimaMedia.size() - 1){
                desc.append(", ");
            }
        }
        this.itensAcimaMediaDescricao = desc.toString();
    }
    public String getItensAcimaDaMediaDescricao(){
        return this.itensAcimaMediaDescricao;
    }
}
