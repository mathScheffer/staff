
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemTest{
    @Test
    public void ItemDeveSerCriadoComoDesequipado(){
        Item item = new Item(1,"Escudo");
        EquipamentoStatus status;
        assertEquals(EquipamentoStatus.DESEQUIPADO, item.getItemStatus());
    }
    
    @Test
    public void DoisItemDeveSerEquipado(){
        Item Escudo = new Item(1,"Escudo");
        Item Espada = new Item(1,"Espada");
        EquipamentoStatus status;
        
        assertEquals(EquipamentoStatus.DESEQUIPADO, Escudo.getItemStatus());
        assertEquals(EquipamentoStatus.DESEQUIPADO, Espada.getItemStatus());
        
        Escudo.setItemStatus(EquipamentoStatus.EQUIPAR);
        Espada.setItemStatus(EquipamentoStatus.EQUIPAR);
        
        assertEquals(EquipamentoStatus.EQUIPADO, Escudo.getItemStatus());
        assertEquals(EquipamentoStatus.EQUIPADO, Espada.getItemStatus());
    }
}
