import java.util.ArrayList;
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PaginadorInventarioTest
{
    @Test
    public void PaginadorInventarioTestDevePaginar(){
        Inventario inventario = new Inventario();
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        ArrayList<Item> itens = new ArrayList<Item>();
        
        Item Arco = new Item(1,"Arco");
        Item Flecha = new Item(2,"flechas");
        Item Adaga = new Item(3,"adagas");
        Item Escudo = new Item(1,"escudo");
        Item Pocao = new Item(1,"poção");
        
        paginador.getInventario().adicionarItem(Arco);
        paginador.getInventario().adicionarItem(Flecha);
        paginador.getInventario().adicionarItem(Adaga);
        paginador.getInventario().adicionarItem(Escudo);
        paginador.getInventario().adicionarItem(Pocao);
        
        paginador.pular(2);
        
        itens.add(Adaga);
        itens.add(Escudo);
        
        assertEquals(itens, paginador.limitar(2) );
    }
}
